import redis from 'redis'
const clientRedis = redis.createClient({
    url:'redis://redis'
});

clientRedis.on('error', (err) => console.log('Redis Client Error', err));

await clientRedis.connect();

export default clientRedis;
