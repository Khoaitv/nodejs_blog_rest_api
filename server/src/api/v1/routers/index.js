import express from "express";
import authRouter from './auth.router.js';
import categoryRouter from './category.router.js';
import postRouter from './post.router.js';
import verifyToken from '../middleware/auth.middleware.js';

const router = express.Router();

router.use('/auth', authRouter);
router.use('/category', verifyToken, categoryRouter);
router.use('/post', verifyToken, postRouter);

router.use((error,req, res, next)=>{
    res.json({
        "status": error.status||500,
        "message": error.message
    })
});

export default router;
