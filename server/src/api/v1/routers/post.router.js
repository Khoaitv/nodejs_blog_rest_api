import express from 'express'
import postController from '../controllers/post.controller.js';

const route = express.Router();

route.get('/', postController.index);
route.post('/create', postController.store);
route.put('/update/:id', postController.update);
route.delete('/destroy/:id', postController.destroy);

export default route;