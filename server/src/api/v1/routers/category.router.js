import express from 'express'
import categoryController from '../controllers/category.controller.js';

const route = express.Router();

route.get('/', categoryController.index);
route.post('/create', categoryController.store);
route.put('/update/:id', categoryController.update);
route.delete('/destroy/:id', categoryController.destroy);

export default route;