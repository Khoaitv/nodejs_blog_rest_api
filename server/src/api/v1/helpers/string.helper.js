import slug from 'slugify'

export const createSlug = string => {
    return slug(string, {
        replacement : '-',
        lower:true
    })
}