import prisma from "../helpers/prisma.js";
export const Category = prisma.category;
export const select = {
    id:true,
    name:true,
    slug: true
}
