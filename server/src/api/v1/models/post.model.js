import prisma from "../helpers/prisma.js";
export const Post = prisma.posts;
export const select = {
    id:true,
    name:true,
    slug: true
}
