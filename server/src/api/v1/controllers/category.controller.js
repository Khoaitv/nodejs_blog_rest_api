import { createCateValidate } from "../validations/category.validate.js";
import createError from "http-errors";
import logger from '../../../config/log.js';
import categoryService from "../services/category.service.js";
import {createSlug} from '../helpers/string.helper.js';

const categoryController = {

    index: async(req, res, next) => {
        try {
            const categories = await categoryService.getAllCategory1();

            res.json(categories);
        } catch (error) {
            next(createError(error));
        }
    },

    store: async(req, res, next) => {
        try {
            const {error} = createCateValidate(req.body);

            if (error) {
                throw createError(error.details[0].message);
            }

            const categoryInfo = {
                name : req.body.name,
                slug : createSlug(req.body.name),
                parent_id: req.body.parent,
            };

            const category = await categoryService.createCategory(categoryInfo);

            logger.info("create category successfully");

            res.json(category);

        } catch (error) {
            logger.error("create category fail with "+ error);
            next(createError(error));
        }
    },

    update: async(req, res,next) => {
        try {
            const {error} = createCateValidate(req.body);

            if (error) {
                throw createError(error.details[0].message);
            }

            const categoryId = parseInt(req.params.id);
            const category = await categoryService.findCategoryById(categoryId);

            if (!category) {
                throw createError("category not found");
            }

            const categoryInfo = {
                name : req.body.name,
                slug : createSlug(req.body.name),
                parent_id: req.body.parent,
            };

            const categoryData =  await categoryService.updateCategory(categoryId, categoryInfo);

            logger.info("update category successfully");
            
            res.json(categoryData);

        } catch (error) {
            logger.error("update category fail with "+ error);
            next(createError(error));
        }
    },

    destroy: async(req, res, next) => {
        try {
            const categoryId = parseInt(req.params.id);
            const category = await categoryService.findCategoryById(categoryId);
    
            if (!category) {
                throw createError("category not found");
            }
            categoryService.destroyOneCategory(categoryId);
            logger.info("delete category successfully");
            res.json({
                code: true,
                message: "remove category success"
            });
        } catch (error) {
            logger.error("delete category fail with "+ error);
            next(createError(error)); 
        }

    },
}

export default categoryController;