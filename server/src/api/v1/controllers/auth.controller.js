import creatError from 'http-errors'
import logger from '../../../config/log.js';
import {registerValidate, loginValidate} from '../validations/auth.validate.js';
import authService from '../services/auth.service.js';
import bcrypt from 'bcrypt';

const authController = {

    // regiter
    register : async(req, res, next) => {
      try {

          const {error} = registerValidate(req.body);
         
          if (error) {
              throw creatError(error.details[0].message);
          }

          const userInfo = {
            name: req.body.username,
            email: req.body.email,
            isAdmin: req.body.admin ? true : false,
            password: await authService.hashUserPassword(req.body.password)
          }

         const user = await authService.register(userInfo);
        
         res.status(201).json(user);
      } catch (error) {
        logger.error("user register account fail with "+ error);
        next(createError(error));
      }
    },

    //login
    login : async(req, res, next) => {
       try{
        const {error} = loginValidate(req.body);

        if (error) {
          throw creatError(error.details[0].message);
        }

        const user = await authService.findUserByEmail(req.body.email);

        if (!user) {
          throw creatError("user not found");
        }

        const validatePassword = await bcrypt.compare(req.body.password, user.password);

        if (!validatePassword) {
          throw creatError("wrong password");
        }

        const accessToken = await authService.createAccessToken(user);

        const {password , ...userInfo} = user; 

        res.status(200).json({userInfo, accessToken});
       } catch(error) {
         logger.error(error);
         next(createError(error));
       }


    }

}

export default authController;