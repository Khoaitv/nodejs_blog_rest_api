import { createPostValidate } from "../validations/post.validate.js";
import createError from "http-errors";
import logger from '../../../config/log.js';
import postService from "../services/post.service.js";
import {createSlug} from '../helpers/string.helper.js';

const postController = {

    index: async(req, res, next) => {
        try {
            const posts = await postService.getAllPost();

            res.json(posts);
        } catch (error) {
            next(createError(error)); 
        }
    },

    store: async(req, res, next) => {
        try {
            const {error} = createPostValidate(req.body);

            if (error) {
                throw createError(error.details[0].message);
            }

            const postInfo = {
                name : req.body.name,
                slug : createSlug(req.body.name),
                categoryId: req.body.category,
                avatar: req.body.avatar,
                images: JSON.stringify(req.body.image),
                authorId : 1,
                description: req.body.description,
                content: req.body.content
            };

            const post = await postService.createCategory(postInfo);

            logger.info("create post successfully");

            res.json(post);

        } catch (error) {
            logger.error("create post fail with "+ error);
            next(createError(error));
        }
    },

    update: async(req, res, next) => {
        try {
            const {error} = createCateValidate(req.body);

            if (error) {
                throw createError(error.details[0].message);
            }

            const postId = parseInt(req.params.id);
            const post = await postService.findCategoryById(postId);

            if (!post) {
                throw createError("post not found");
            }

            const postInfo = {
                name : req.body.name,
                slug : createSlug(req.body.name),
                categoryId: req.body.category,
                avatar: req.body.avatar,
                images: JSON.stringify(req.body.image),
                authorId : 1,
                description: req.body.description,
                content: req.body.content
            };

            const postData =  await postService.updatePost(postId, postInfo);

            logger.info("update post successfully");
            
            res.json(postData);

        } catch (error) {
            logger.error("update post fail with "+ error);
            next(createError(error));
        }
    },

    destroy: async(req, res, next) => {
        try {
            const postId = parseInt(req.params.id);
            const post = await postService.findPostById(postId);
    
            if (!post) {
                throw createError("post not found");
            }
            postService.destroyOnePost(postId);
            logger.info("delete post successfully");
            res.json({
                code: true,
                message: "remove post success"
            });
        } catch (error) {
            logger.error("delete post fail with "+ error);
            next(createError(error));
        }

    },
}

export default postController;