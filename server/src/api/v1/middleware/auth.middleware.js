import jwt from 'jsonwebtoken'
import createError from 'http-errors';

const verifyToken = (req, res, next) =>{
    const token = req.header("Authorization");

    if (!token) {
        return next(createError.Unauthorized());
    } 

    const accessToken = token.split(" ")[1];
    jwt.verify(accessToken, process.env.JWT_ACCESS_KEY,(err, user)=>{
        if(err) {
            return next(createError.Unauthorized());
        }

        req.user = user;
        next();
    })
}

export default verifyToken;