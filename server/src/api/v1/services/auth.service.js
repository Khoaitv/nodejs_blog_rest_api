import createError from "http-errors";
import { select, User } from "../models/user.model.js";
import bcrypt from "bcrypt";
import jwt from 'jsonwebtoken';

const authService = {

  /**
   * register user
   * 
   * @param {object} data 
   * @returns 
   */
    register: async(data) => {
      try{
        return await User.create({
            data
        });

      } catch(error) {
        throw createError(error);
      }
    },

    /**
     * find user by email
     * 
     * @param {string} email 
     * @returns 
     */
    findUserByEmail: async(email) =>{
      try {
        return await User.findUnique({
          where:{
            email
          },
          select
        })
      } catch (error) {
        throw createError(error);
      }
    },

    hashUserPassword: async(password) => {
      const salt = await bcrypt.genSalt(10);

      return await bcrypt.hash(password, salt);
    },

    createAccessToken: async(user)=>{
      return jwt.sign({
        id:user.id,
        admin: user.isAdmin
      },process.env.JWT_ACCESS_KEY,{
        expiresIn:'30s'
      });
    },

    createRefreshToken: async(user)=>{
      return jwt.sign({
        id:user.id,
        admin: user.isAdmin
      },process.env.JWT_REFRESH_TOKEN,{
        expiresIn:'1y'
      });
    }
}

export default authService;