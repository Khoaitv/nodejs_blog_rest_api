import createError from 'http-errors';
import { select, Category } from "../models/category.model.js";


const categoryService = {
    getAllCategory: async(data = "") => {
        try {
            return await Category.findMany()
        } catch (error) {
            throw createError(error);
        }
    },

    createCategory: async(data) => {
        try {
            return await Category.create({
                data
            });
        } catch (error) {
            throw createError(error);
        }
    },

    updateCategory: async(id, data) => {
        try {
            return await Category.update({
                where:{
                    id
                },
                data
            });
        } catch (error) {
            throw createError(error);
        }
    },

    destroyOneCategory: async(id) => {
       try {
        return await Category.delete({
            where: {
              id
            },
          })
       } catch (error) {
        throw createError(error);
       }
    },

    destroyManyCategory: (data) =>{
        // const deleteUsers = await prisma.user.deleteMany({
        //     where: {
        //       email: {
        //         contains: 'prisma.io',
        //       },
        //     },
        //   })
    },

    changeStatusOneCategory: (data) => {

    },

    changeStatusManyCategory: (data) => {

    },
    findCategoryByName: async(name) => {
        try {
            return await Category.findUnique({
                where: { name },
                select
              })
        } catch (error) {
            throw createError(error);
        }
    },

    findCategoryById: async(id) => {
        try {
            return await Category.findUnique({
                where: { id:id },
              })
        } catch (error) {
            throw createError(error);
        }
    }
}

export default categoryService;