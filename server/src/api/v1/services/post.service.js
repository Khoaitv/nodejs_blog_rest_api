import createError from 'http-errors';
import { select, Post } from "../models/post.model.js";


const postService = {
    getAllPost: async(data = "") => {
        try {
            return await Post.findMany()
        } catch (error) {
            throw createError(error);
        }
    },

    createPost: async(data) => {
        try {
            return await Post.create({
                data
            });
        } catch (error) {
            throw createError(error);
        }
    },

    updatePost: async(id, data) => {
        try {
            return await Post.update({
                where:{
                    id
                },
                data
            });
        } catch (error) {
            throw createError(error);
        }
    },

    destroyOnePost: async(id) => {
       try {
        return await Post.delete({
            where: {
              id
            },
          })
       } catch (error) {
        throw createError(error);
       }
    },

    destroyManyPost: (data) =>{
        // const deleteUsers = await prisma.user.deleteMany({
        //     where: {
        //       email: {
        //         contains: 'prisma.io',
        //       },
        //     },
        //   })
    },

    changeStatusOnePost: (data) => {

    },

    changeStatusManyPost: (data) => {

    },
    findPostByName: async(name) => {
        try {
            return await Post.findUnique({
                where: { name },
                select
              })
        } catch (error) {
            throw createError(error);
        }
    },

    findPostById: async(id) => {
        try {
            return await Post.findUnique({
                where: { id:id },
              })
        } catch (error) {
            throw createError(error);
        }
    }
}

export default postService;