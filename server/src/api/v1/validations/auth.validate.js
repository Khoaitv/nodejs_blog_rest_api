import joi from 'joi'

const registerValidate = data => {
    const schema = joi.object({
        username: joi.string().min(3).max(30).required(),
        email: joi.string().email().required(),
        password: joi.string().min(3).max(11).required(),
        admin:joi.any()
    });

    return schema.validate(data);
}

const loginValidate = data => {
    const schema = joi.object({
        email: joi.string().email().required(),
        password: joi.string().required(),
    });

    return schema.validate(data);
}

export {registerValidate, loginValidate};


