import Joi from 'joi'
import postService from '../services/post.service.js';

export const createPostValidate = (data) => {
    const checkNameUnique = async (value, helpers)=>{
        const post = await postService.findCategoryByName(value);

        if (post) {
            console.log(helpers);
            return helpers.error('name da ton tai');
        }

        return value;
    }

    const schema = Joi.object({
        name : Joi.string().required().custom(checkNameUnique, "check name unique"),
        avatar: Joi.string().required(),
        description: Joi.any(),
        content: Joi.any(),
        category: Joi.number().required(),
        images: Joi.any()
    });



    return schema.validate(data);
}

