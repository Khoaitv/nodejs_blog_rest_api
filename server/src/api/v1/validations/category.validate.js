import Joi from 'joi'
import categoryService from '../services/category.service.js';

export const createCateValidate = (data) => {
    const checkNameUnique = async (value, helpers)=>{
        const category = await categoryService.findCategoryByName(value);

        if (category) {
            console.log(helpers);
            return helpers.error('name da ton tai');
        }

        return value;
    }

    const schema = Joi.object({
        name : Joi.string().required().custom(checkNameUnique, "check name unique"),
        parent: Joi.number()
    });



    return schema.validate(data);
}

