import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import helmet from 'helmet'
import 'dotenv/config'
import morgan from 'morgan'
import router from './api/v1/routers/index.js';
import './config/redis.js'


const app = express()
const port = 3000
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

//cors
app.use(cors())
//helmet
app.use(helmet());
//morgan
app.use(morgan('combined'))
//redis


app.get('/', (req, res) => {
  res.send('Hello World!');
})

app.use('/api/v1', router);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})