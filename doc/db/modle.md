https://www.prisma.io/docs/concepts/components/prisma-schema/data-model
- Cu phap tao table:
    model name_table {
        //field
    }
1. table name
- name_table se la ten bang trong db. 
- chung ta co the thay doi ten table voi : @@map("comments")
- Using @map and @@map to rename fields and models
2. field
2.1 type field
2.1.1: scalar type: string, int, ...
https://www.prisma.io/docs/reference/api-reference/prisma-schema-reference#model-field-scalar-types
2.1.2: relation
https://www.prisma.io/docs/concepts/components/prisma-schema/relations 
2.1.3: Native types mapping
- type of field in db
2.1.4 Type modifiers

[]  Make a field a list
?  Make a field optional

2.1.5 Defining attributes

Attributes modify the behavior of fields or model blocks. The following example includes three field attributes (@id , @default , and @unique ) and one block attribute (@@unique ):
ex:
model User {
  id        Int     @id @default(autoincrement())
  firstName String
  lastName  String
  email     String  @unique
  isAdmin   Boolean @default(false)

  @@unique([firstName, lastName])
}